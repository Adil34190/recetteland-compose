echo -e "\e[32mUpdating repos"
sudo apt-get update
echo -e "\e[32mInstalling git..."
sudo apt install git-all
echo -e "\e[32mInstalling intellij..."
sudo snap install intellij-idea-community --classic
echo -e "\e[32mInstalling Docker..."
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
echo -e "\e[32mInstalling Docker Compose..."
sudo curl -L "https://github.com/docker/compose/releases/download/1.28.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
echo -e "\e[32mInstalling Open JDK 8..."
sudo apt-get install openjdk-8-jdk
echo -e "\e[32mInstalling postman..."
sudo snap install postman
echo -e "\e[32mInstalling node js..."
sudo snap install node --classic --channel=14
